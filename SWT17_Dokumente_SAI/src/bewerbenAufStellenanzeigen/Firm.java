package bewerbenAufStellenanzeigen;

import java.awt.Image;
import java.util.*;

public class Firm {
	private String name;
	private Image logo;
	public ArrayList<JobAd> jobs;
	public HashSet<Location> locations;
	
	//fehlende Methoden//
	public void addLocation(Location location){
		
	}
	public void removeLocation(Location location){
		
	}
	public void addJobAd(JobAd jon){
		
	}
	public void removeJobAd(JobAd jon){
		
	}
	
	
	//Getter- und Setter-Methoden
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Image getLogo() {
		return logo;
	}
	public void setLogo(Image logo) {
		this.logo = logo;
	}
}
