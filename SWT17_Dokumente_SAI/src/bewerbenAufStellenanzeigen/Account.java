package bewerbenAufStellenanzeigen;

import java.util.*;

public class Account {
	private String username;
	private String password;
	private String name;
	private String surname;
	private String email;
	private Boolean eingeloggt;
	public HashSet<Skill> skills;
	public LinkedList<JobApplication> application;
	
	public Boolean login(){
		return eingeloggt;
	}
	
	//fehlende Methoden//
	public void addSkills(Skill skill){
		
	}
	public void removeSkills(Skill skill){
		
	}
	public void addJobApplication(JobApplication jobApp){
		application.add(jobApp);
	}
	public void removeJobApplication(JobApplication jobApp){
		
	}
    
	//Getter- und Setter-Methoden//
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getEingeloggt() {
		return eingeloggt;
	}

	public void setEingeloggt(Boolean eingeloggt) {
		this.eingeloggt = eingeloggt;
	}
	
	
}
