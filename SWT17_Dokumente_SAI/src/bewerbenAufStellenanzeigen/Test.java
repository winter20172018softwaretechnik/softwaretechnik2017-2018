package bewerbenAufStellenanzeigen;

import java.util.LinkedList;

public class Test {
	public static void main(String[] args){
			JobAd job1 = new JobAd();
			job1.setFullTime(true);
			job1.setName("Apotheker");
		
			JobAd job2 = new JobAd();
			job2.setFullTime(true);
			job2.setName("Detektiv");
			
			JobAd job3 = new JobAd();
			job3.setFullTime(true);
			job3.setName("Florist");
			
			JobAd job4 = new JobAd();
			job4.setFullTime(true);
			job4.setName("Informatiker");
			
			JobAd job5 = new JobAd();
			job5.setFullTime(true);
			job5.setName("Kaufmann");
			
			JobAd job6 = new JobAd();
			job6.setFullTime(false);
			job6.setName("Apotheker");
		
			JobAd job7 = new JobAd();
			job7.setFullTime(false);
			job7.setName("Detektiv");
			
			JobAd job8 = new JobAd();
			job8.setFullTime(false);
			job8.setName("Florist");
			
			JobAd job9 = new JobAd();
			job9.setFullTime(false);
			job9.setName("Informatiker");
			
			JobAd job10 = new JobAd();
			job10.setFullTime(false);
			job10.setName("Kaufmann");
			
			Account acc = new Account();
			acc.setUsername("Sai");
			acc.setPassword("123");
			acc.setName("Ma");
			acc.setSurname("Sai");
			acc.setEmail("23333@gmail.com");
			acc.setEingeloggt(true);
			
			JobApplication job = new JobApplication();
			job.status = null;
			job.bewerben(job9);
			
			acc.application = new LinkedList();
			acc.application.add(job);
			
			for(int i = 0; i < acc.application.size() ; i++){
				System.out.println("Name des Jobs :" + acc.application.get(i).job.getName());
				System.out.println("Fulltime :" + acc.application.get(i).job.getFullTime());
				System.out.println("Status der Applikation :" +  acc.application.get(i).status);
			}
	}
}
