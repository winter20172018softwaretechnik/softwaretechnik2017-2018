package stellenvorschlaegenUndStellenanzeigen;

public class Skill {
	private String name;
	private int niveau;
	
	//Getter- und Setter-Methoden//
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNiveau() {
		return niveau;
	}
	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}
}
