package stellenvorschlaegenUndStellenanzeigen;

import java.util.*;

public class Kriterium {
	private String filterkriterium;
	private String sortierkriterium;
	
	
	public ArrayList<JobAd> filterkriteriumAuswaehlen(String filterkriterium,ArrayList<JobAd> jobs){
		ArrayList<JobAd> foundJobs = new ArrayList<JobAd>();
		for(int i = 0; i < jobs.size(); i++){
			if(jobs.get(i).getKriterium().equals(filterkriterium)){
				foundJobs.add(jobs.get(i));
			}
		}
		return foundJobs;
	}
	
	public ArrayList<JobAd> sortierkriteriumAuswaehlen(String sortierkriterium,ArrayList<JobAd> jobs){
		ArrayList<JobAd> foundJobs = new ArrayList<JobAd>();
		for(int i = 0; i < jobs.size(); i++){
			if(jobs.get(i).getKriterium().equals(sortierkriterium)){
				foundJobs.add(jobs.get(i));
			}
		}
		return foundJobs;
	}

	//Getter- und Setter-Methoden//
	public String getFilterkriterium() {
		return filterkriterium;
	}

	public void setFilterkriterium(String filterkriterium) {
		this.filterkriterium = filterkriterium;
	}

	public String getSortierkriterium() {
		return sortierkriterium;
	}

	public void setSortierkriterium(String sortierkriterium) {
		this.sortierkriterium = sortierkriterium;
	}
	
}
