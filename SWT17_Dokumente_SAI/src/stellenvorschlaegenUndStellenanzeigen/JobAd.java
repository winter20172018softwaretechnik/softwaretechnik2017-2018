package stellenvorschlaegenUndStellenanzeigen;

import java.util.*;

public class JobAd {
	private String name;
	private Boolean fullTime;
	private Kriterium kriterium;
	public Firm firm;
	public LinkedList<Benefit> benefits;
	public HashSet<Location> locations;
	public HashSet<Skill> requiredSkills;
	
	public void addBenefit(Benefit addedBenefit){
		benefits.add(addedBenefit);
	}
	public void removeBenefit(int position){
		benefits.remove(position);
	}
	
	//fehlende Methoden//
	public void addSkill(Skill skill){
		
	}
	public void removeSkill(int position){
		
	}
	public void addFirm(){
		
	}
	public void removeFirm(){
		
	}
	public void addLocation(Location location){
		
	}
	public void removeLocation(Location location){
		
	}
	
	//Getter- und Setter-Methoden//
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getFullTime() {
		return fullTime;
	}
	public void setFullTime(Boolean fullTime) {
		this.fullTime = fullTime;
	}
	public Kriterium getKriterium() {
		return kriterium;
	}
	public void setKriterium(Kriterium kriterium) {
		this.kriterium = kriterium;
	}
}
