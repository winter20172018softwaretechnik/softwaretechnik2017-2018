package stellenvorschlaegenUndStellenanzeigen;

public class Profi {
	private Kriterium kriterium;
	private int alter;
	private int handynummer;
	
	
	
	//Getter- und Setter-Methoden//
	public Kriterium getKriterium() {
		return kriterium;
	}
	public void setKriterium(Kriterium kriterium) {
		this.kriterium = kriterium;
	}
	public int getAlter() {
		return alter;
	}
	public void setAlter(int alter) {
		this.alter = alter;
	}
	public int getHandynummer() {
		return handynummer;
	}
	public void setHandynummer(int handynummer) {
		this.handynummer = handynummer;
	}
}
