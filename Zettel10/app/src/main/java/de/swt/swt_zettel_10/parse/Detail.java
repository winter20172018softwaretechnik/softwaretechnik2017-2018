package de.swt.swt_zettel_10.parse;

/**
 * Created by Sai on 2018/1/27.
 */

public class Detail {
        private int id;
        private String industry_type;
        private String title;
        private String description;
        private String creation;
        private String contract_type;
        private String work_type;
        private int seats;
        private int required_work_experience;
        private int required_leadership_experience;
        private String current_state;
        private Company company;
        private Member supervising_hrmember;
        private Member maintaining_hrmembers;





    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIndustry_type() {
        return industry_type;
    }

    public void setIndustry_type(String industry_type) {
        this.industry_type = industry_type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreation() {
        return creation;
    }

    public void setCreation(String creation) {
        this.creation = creation;
    }

    public String getContract_type() {
        return contract_type;
    }

    public void setContract_type(String contract_type) {
        this.contract_type = contract_type;
    }

    public String getWork_type() {
        return work_type;
    }

    public void setWork_type(String work_type) {
        this.work_type = work_type;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public int getRequired_work_experience() {
        return required_work_experience;
    }

    public void setRequired_work_experience(int required_work_experience) {
        this.required_work_experience = required_work_experience;
    }

    public int getRequired_leadership_experience() {
        return required_leadership_experience;
    }

    public void setRequired_leadership_experience(int required_leadership_experience) {
        this.required_leadership_experience = required_leadership_experience;
    }

    public String getCurrent_state() {
        return current_state;
    }

    public void setCurrent_state(String current_state) {
        this.current_state = current_state;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Member getSupervising_hrmember() {
        return supervising_hrmember;
    }

    public void setSupervising_hrmember(Member supervising_hrmember) {
        this.supervising_hrmember = supervising_hrmember;
    }

    public Member getMaintaining_hrmembers() {
        return maintaining_hrmembers;
    }

    public void setMaintaining_hrmembers(Member maintaining_hrmembers) {
        this.maintaining_hrmembers = maintaining_hrmembers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
