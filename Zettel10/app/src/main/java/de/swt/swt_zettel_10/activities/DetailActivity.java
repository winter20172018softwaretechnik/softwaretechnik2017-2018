package de.swt.swt_zettel_10.activities;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import de.swt.swt_zettel_10.server.DownloadCompleteListener;
import de.swt.swt_zettel_10.server.ServerComm;

public class DetailActivity extends ListActivity implements DownloadCompleteListener {

    private ProgressDialog mProgressDialog;
    //Hier einen privaten globalen Adapter anlegen

    private void startDownload(int id){
        new ServerComm(this).execute("https://eswt.enwork.de/api/data/vacancies/"+id+"/");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (isNetworkConnected()) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Bitte warten...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            //Hier den Adapter anlegen
            int position = 0; //position auf die übergebene ID setzen

            startDownload(position);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Kein Internet")
                    .setMessage("Anscheinend hast du das Internet gelöscht! WP, dude!")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    @Override
    public void downloadComplete(String jsonString) {
        //Hier parsen und den Adapter befüllen

        if (mProgressDialog != null) {
            mProgressDialog.hide();
        }
    }
}
