package de.swt.swt_zettel_10.parse;

import android.util.Log;
import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sai on 2018/1/27.
 */

public class JSON_transfer {
    public static Detail transferDetails(JSONObject details){
        Detail detail = new Detail();
       try{
           JSONObject company = details.getJSONObject("company");
           Company c = new Company();
           c.setId(company.getInt("id"));
           c.setName(company.getString("name"));
           c.setDomain(company.getString("domain"));
           c.setLogo(company.getString("logo"));
           c.setCompany_type(company.getString("company_type"));
           c.setCompany_size(company.getString("company_size"));
           c.setPrimary_color(company.getString("primary_color"));
           c.setAccent_color(company.getString("accent_color"));

           JSONObject supervising_hrmember = details.getJSONObject("supervising_hrmember");
           Member s = new Member();
           s.setId(supervising_hrmember.getInt("id"));
           s.setAvatar(supervising_hrmember.getString("avatar"));
           s.setPosition(supervising_hrmember.getString("position"));
           s.setUser(supervising_hrmember.getInt("user"));
           s.setCompany(supervising_hrmember.getInt("company"));

           JSONObject maintaining_hrmembers = details.getJSONObject("maintaining_hrmembers");
           Member m = new Member();
           m.setId(supervising_hrmember.getInt("id"));
           m.setAvatar(supervising_hrmember.getString("avatar"));
           m.setPosition(supervising_hrmember.getString("position"));
           m.setUser(supervising_hrmember.getInt("user"));
           m.setCompany(supervising_hrmember.getInt("company"));




           detail.setId(details.getInt("id"));
           detail.setIndustry_type(details.getString("industry_type"));
           detail.setTitle(details.getString("title"));
           detail.setDescription(details.getString("description"));
           detail.setCreation(details.getString("creation"));
           detail.setContract_type(details.getString("contract_type"));
           detail.setWork_type(details.getString("work_type"));
           detail.setSeats(details.getInt("seats"));
           detail.setRequired_work_experience(details.getInt("required_work_experience"));
           detail.setRequired_leadership_experience(details.getInt("required_leadership_experience"));
           detail.setCurrent_state(details.getString("current_state"));
           detail.setCompany(c);
           detail.setSupervising_hrmember(s);
           detail.setMaintaining_hrmembers(m);


       } catch (final JSONException e) {
           Log.e("errorD", "Json transfer error");
       }
            return  detail;
    }

    public static Vacancy[] transferVancancy(JSONArray vacancy){
        Vacancy[] vacancies = new Vacancy[vacancy.length()];
            try{
                for(int i = 0; i <vacancy.length(); i++ ){
                    JSONObject company = vacancy.getJSONObject(i).getJSONObject("company");
                    Company c = new Company();
                    c.setName(company.getString("name"));

                    vacancies[i].setTitle(vacancy.getJSONObject(i).getString("title"));
                    vacancies[i].setSeats(vacancy.getJSONObject(i).getInt("seats"));
                    vacancies[i].setCompany(c);
                }
            }catch (final JSONException e){
                Log.e("errorV", "Json transfer error");
            }

            return vacancies;
    }
}
