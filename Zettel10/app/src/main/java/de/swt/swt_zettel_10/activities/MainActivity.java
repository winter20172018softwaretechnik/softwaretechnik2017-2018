package de.swt.swt_zettel_10.activities;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ListView;
import de.swt.swt_zettel_10.server.DownloadCompleteListener;
import de.swt.swt_zettel_10.server.ServerComm;

public class MainActivity extends ListActivity implements DownloadCompleteListener {
    private ProgressDialog  mProgressDialog;
    //Globale private Adaptervariable hier anlegen

    private void startDownload(){
        new ServerComm(this).execute("https://eswt.enwork.de/api/data/vacancies/");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //Hier Adapter initialisieren
        if (isNetworkConnected()) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Bitte warten...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();

            startDownload();
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Kein Internet")
                    .setMessage("Anscheinend hast du das Internet gelöscht! WP, dude!")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE); // 1
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo(); // 2
        return networkInfo != null && networkInfo.isConnected(); // 3
    }

    @Override
    public void downloadComplete(String jsonString) {
        //Hier parsen

        //Hier Adapter updaten
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        //Hier die position an die neue Activity übergeben und diese Starten
        //Aufpassen: Die ID's der Jobs starten bei 1 statt 0

    }
}
