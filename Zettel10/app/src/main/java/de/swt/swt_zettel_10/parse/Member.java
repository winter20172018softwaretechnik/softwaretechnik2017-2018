package de.swt.swt_zettel_10.parse;

/**
 * Created by Sai on 2018/1/27.
 */

public class Member {
    private int id;
    private String avatar;
    private String position;
    private int user;
    private int company;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getCompany() {
        return company;
    }

    public void setCompany(int company) {
        this.company = company;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String positon) {
        this.position = positon;
    }
}
