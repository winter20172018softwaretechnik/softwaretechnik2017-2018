package de.swt.swt_zettel_10.parse;

/**
 * Created by Sai on 2018/1/27.
 */

public class Vacancy {
    private String title;
    private int seats;
    private Company company;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
