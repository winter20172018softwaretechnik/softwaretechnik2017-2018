package de.swt.swt_zettel_10.server;

public interface DownloadCompleteListener {
    void downloadComplete(String jsonString);
}
