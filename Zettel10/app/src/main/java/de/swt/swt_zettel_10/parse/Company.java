package de.swt.swt_zettel_10.parse;

/**
 * Created by Sai on 2018/1/27.
 */

import android.graphics.Color;
import android.media.Image;


public class Company {
        private int id;
        private String name;
        private String domain;
        private String logo;
        private String company_type;
        private String company_size;
        private String primary_color;
        private String accent_color;

        public int getId() {
                return id;
        }

        public void setId(int id) {
                this.id = id;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getDomain() {
                return domain;
        }

        public void setDomain(String domain) {
                this.domain = domain;
        }

        public String getLogo() {
                return logo;
        }

        public void setLogo(String logo) {
                this.logo = logo;
        }

         public String getCompany_type() {
         return company_type;
        }

        public void setCompany_type(String company_type) {
          this.company_type = company_type;
         }

        public String getCompany_size() {
        return company_size;
        }

         public void setCompany_size(String company_size) {
        this.company_size = company_size;
         }

        public String getPrimary_color() {
        return primary_color;
        }

         public void setPrimary_color(String primary_color) {
        this.primary_color = primary_color;
         }

         public String getAccent_color() {
        return accent_color;
         }

         public void setAccent_color(String accent_color) {
        this.accent_color = accent_color;
         }
}
